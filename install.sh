#! /bin/bash

docker-compose build --no-cache
docker-compose up -d
docker-compose exec app php artisan key:generate
docker-compose exec app php artisan migrate:fresh
open http:localhost:8088