<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//API route for register new user
Route::post('/register', [UserController::class, 'register'])->name('register');
//API route for login user
Route::post('/login', [UserController::class, 'login'])->name('login');
// API route for logout user
Route::post('/logout', [UserController::class, 'logout'])->middleware('auth:sanctum')->name('logout');
// API route for Update
Route::put('/user/{id}', [UserController::class, 'update'])->middleware('auth:sanctum')->name('update');
// API route for view User
Route::get('/user', [UserController::class, 'about'])->middleware('auth:sanctum')->name('update');