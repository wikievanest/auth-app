<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Auth as Auth;

class UserController extends BaseController
{
    // User Register
    public function register(Request $request) {
        $validator = Validator::make($request->all(),[
            'username' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'name' => 'required',
            'password' => 'required|string|min:8',
            'password' => 'required|string',
        ]);

        if($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $input = $request->all();

        $input['password'] = Hash::make($input['password']);
        
        $user = User::create($input);

        $success['token'] = $user->createToken('auth_token')->plainTextToken;
        $success['username'] = $user->username;

        return $this->sendResponse($success, 'User Registered Successfully');

    }

    // User Login
    public function login(Request $request) {

        if (!Auth::attempt($request->only('email', 'password')))
        {
            return $this->sendError('Unauthorized', ['error' => 'Unauthorized']);
        }

        $user = User::where('email', $request['email'])->firstOrFail();
        $success['username'] = $user->username;;
        $success['token'] = $user->createToken('auth_token')->plainTextToken;
        return $this->sendResponse($success, 'User login successfully');
    }

    // User Logout
    public function logout(Request $request)
    {
        Auth::user()->tokens->each(function($token, $key) {
            $token->delete();
        });
    
        return $this->sendResponse([], 'Successfully logged out');
    }

    // User view
    public function about(Request $request)
    {    
        $user = $request->user();
        return $this->sendResponse($user, 'Successfully fetch user data');
    }

    // User Update
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|string',
        ]);

        if($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user = User::find($id);
        if($user != NULL) {
            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->phone = $request->get('phone');
            if($user->save()) {
                return $this->sendResponse($request->all(), 'User update Successfully');
            } else {
                return $this->sendError('Update Error.', 'User update failed!');
            }
        }

        
        
    }
}
